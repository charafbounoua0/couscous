from django.db import models
from django.contrib.auth import get_user_model

class Profile(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, default=1, blank=True)
    profile_pic = models.FileField(blank=True)
    address = models.CharField(max_length=255, blank=True)
    state = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)
    country = models.CharField(max_length=255, blank=True)
    phone = models.CharField(max_length=100, blank=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def user__username(self):
        return self.user.username

    def __str__(self):
        return self.user.username
from rest_framework import serializers
from .models import Profile
from django.contrib.auth import get_user_model

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = (
            'id',
            'is_superuser',
            'username',
            'first_name',
            'last_name',
            'email',
            'date_joined',
        )

class ProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    class Meta:
        model = Profile
        fields = (
            'id',
            'profile_pic',
            'address',
            'state',
            'city',
            'country',
            'phone',
            'updated_at',
            'user'
        )

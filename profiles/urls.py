from django.urls import path
from .views import ProfileDetail

urlpatterns = [
    path('<user__username>', ProfileDetail.as_view())
]
from django.contrib import admin
from django.urls import path, include
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from django.contrib.auth.views import PasswordResetConfirmView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/api-auth/', include('rest_framework.urls')),
    path('api/v1/account/', include('rest_auth.urls')),
    path('api/v1/account/', include('profiles.urls')),
    path('api/v1/signup/', include('rest_auth.registration.urls')),
    path('api/v1/account/', include('allauth.urls')),
    path('api/v1/auth/token/', obtain_jwt_token),
    path('api/v1/auth/token/refresh/', refresh_jwt_token),
    path('api/v1/auth/token/verify/', verify_jwt_token),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
